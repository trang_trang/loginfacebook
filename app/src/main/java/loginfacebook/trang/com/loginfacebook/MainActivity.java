package loginfacebook.trang.com.loginfacebook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.signup);
        String mystring = "Sign Up for Facebook";
        SpannableString content = new SpannableString(mystring);
        content.setSpan(new UnderlineSpan(),0, mystring.length(), 0);
        text.setText(content);
    }
}
